module gitlab.com/witchbrew/september/go/reply-migrate

go 1.15

require (
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.0.0
	gitlab.com/witchbrew/go/migrate v0.0.0-20201003212111-9a9b0b6e96c2
	gitlab.com/witchbrew/go/migratecliutils v0.0.0-20201003212319-d0131f0a96ba
)
