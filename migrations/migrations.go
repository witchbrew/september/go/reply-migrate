package migrations

import (
	"context"
	"gitlab.com/witchbrew/go/migrate"
	"gitlab.com/witchbrew/go/migrate/postgres"
)

var answersTableUpQuery = `
CREATE TABLE answers
(
    id BIGSERIAL PRIMARY KEY,
    question_id VARCHAR NOT NULL
)
`

var answersTableDownQuery = "DROP TABLE answers"

var openEndedAnswersTableUpQuery = `
CREATE TABLE open_ended_answers
(
    answer_id BIGINT PRIMARY KEY,
    text TEXT NOT NULL
)
`

var openEndedAnswesTableDownQuery = "DROP TABLE open_ended_answers"

var closeEndedAnswersTableUpQuery = `
CREATE TABLE close_ended_answers
(
    answer_id BIGINT NOT NULL,
    option_index INTEGER NOT NULL,
    PRIMARY KEY(answer_id, option_index)
)
`

var closeEndedAnswesTableDownQuery = "DROP TABLE close_ended_answers"

var Migrations = migrate.Migrations{
	{
		Up: func(ctx context.Context) error {
			db := postgres.ContextDB(ctx)
			_, err := db.Exec(openEndedAnswersTableUpQuery)
			if err != nil {
				return err
			}
			_, err = db.Exec(closeEndedAnswersTableUpQuery)
			if err != nil {
				return err
			}
			_, err = db.Exec(answersTableUpQuery)
			if err != nil {
				return err
			}
			return nil
		},
		Down: func(ctx context.Context) error {
			db := postgres.ContextDB(ctx)
			_, err := db.Exec(openEndedAnswesTableDownQuery)
			if err != nil {
				return err
			}
			_, err = db.Exec(closeEndedAnswesTableDownQuery)
			if err != nil {
				return err
			}
			_, err = db.Exec(answersTableDownQuery)
			if err != nil {
				return err
			}
			return nil
		},
	},
}
